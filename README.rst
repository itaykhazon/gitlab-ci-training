******************
gitlab-ci-training
******************
Training for setting up ci with gitlab (using linux runners).

###############
Getting started
###############
First, checkout tag ``basic_project`` and take a look at the repo, we give as a project a simple project that implements a (poor)
graph data structure.

Make sure you can compile and run the unit test attached to it using ``cmake . && make``, the unit tests
should compile automatically and be in ``./tests/test_sanity``.
We'll use this project as our baseline that we add CI to, configure runners for it, and in general make it
better (future versions will replace this UT infrastructure with gtest, run our CI in dockers,add system
test ect.)

:Warning: Make sure you fork the project and work on a clean version, so your changes don't mess up the project for everybody.

############################
Setting up a basic pipelines
############################
Read the `gitlab-ci guide`_ on getting started with gitlab-ci, until "Configuring a Runner" section
(we'll cover that in the next step).

.. _gitlab-ci guide: https://docs.gitlab.com/ee/ci/quick_start/

-------------------------
So what do we need to do?
-------------------------
As you saw in the guide, your entire ci configuration is written in a .gitlab-ci.yml file.

.yml files are pretty simple:

.. code-block:: yaml

    # These are our stages. every job belongs to a staage.
    stages:
      - build
      - test
      - deploy

    # This is a job named "build"
    build:
      stage: build # It will run in the build stage
      script: # This is the actual code the job will run
        - echo "Running job $CI_JOB_NAME"
        - make all

    test:
      stage: test
      script:
        - exit 0 # Its ok to use these as placeholders, just to define a job.
      dependencies:
        - build # This defines that test can't run if "build" didn't succeed

    deploy_dev:
      stage: deploy
      script:
        - echo "Deploying project"
        - python setup.py sdist
        - twine upload dist/*
      dependencies:
        - test

**Exercise:** 

* Add to the project a .gitlab-ci.yml with:

  - a build job that builds the archive
  - test job that runs the ut.
  
* Push it to a new branch, then go to CI/CD tab in your project and see what happened.

:Note: `The gitlab-ci.yml reference`_ is a great source for keywords and working with your ci configuration.
:Note: To avoid commiting just to see syntax errors in the gitlab-ci.yml file, use the CI Lint tool found in ``https://<URL to your project>/-/ci/lint``

.. _The gitlab-ci.yml reference: https://docs.gitlab.com/ee/ci/yaml

###########################
Configuring a gitlab-runner
###########################
As you probably saw in the last section, our pipeline was created but didn't run. That is because the
gitlab-ci pipelines run inside gitlab runners.

The runners are computers, vms or dockers that have a gitlab-runner binary installed, and are connected to your
project. When you have a connected runner, gitlab can ask it to run pipelines.

Read the `step-by-step guide`_ on installing a gitlab runner on your linux machine, and
the `guide to register your runner`_.

.. _step-by-step guide: https://docs.gitlab.com/runner/install/linux-manually.html
.. _guide to register your runner: https://docs.gitlab.com/runner/register/index.html

**Exercise:** 

* Install and register a runner for your project, then rerun the pipeline from the last excercise.
  - Make sure you add tags the specify what this runner can do.
* Once you have a working pipline, improve your ci file:
  - The pipeline should run only on commits to master and dev, and on merge requests.

################################
Advanced: CI/CD with a container
################################

:Note: Please complete the docker tutorial before doing this part.

------------
Introduction
------------
In this section you'll learn:

* How to make a docker gitlab-runner that runs on locally crafted image
* How to setup `GitLab Pages`_
* How to deploy your documentation in a CI/CD method

.. _GitLab Pages: https://about.gitlab.com/stages-devops-lifecycle/pages/

Checkout tag ``basic_project_docs`` and look at the ``docs`` directory.
The ``docs`` directory contains files that will help us generate a documentation for the project, using `Doxygen`.
The ``docs/CMakeLists.txt`` file contains a target that does that already.

If you have already installed `Doxygen` on your machine, you can try to build the documentation by invoking the following commands:

.. code-block:: bash

    mkdir build-docs && cd build-docs
    cmake ..
    cmake --build . --target Doxygen

After building it, open ``build-docs/docs/doxygen/html/index.html`` on your browser and you'll see a full html site
containing all the project's documentation.

-----------------------
Creating a docker image
-----------------------
Now, let's create a docker image that will contain all the needed binaries to build the documentation.

**Exercise:**

Create a dockerfile and build an image called `docs:latest`.

Tips and hints:

* Use the `rikorose/gcc-cmake:latest` as a base image.
* Make sure you have installed the following packages:
    * Flex
    * Bison
    * Doxygen

---------------------------------
Registering the new docker runner
---------------------------------
Note that our newly created image can also build the tests; So we can create a runner and test it on the
previously created `.gitlab-ci.yml` file.

**Exercise:**

* Register a new gitlab-runner that we'll use our new image.
* Make sure that the current CI runs on it.

Tips and hints:

* You can follow the following `guide`_. Remember to select ``docker`` executor.
* You'll see that gitlab will fail the pipeline because it can't find the image. This can be fixed
  by the `/etc/gitlab-runner/config.toml` file and under `[runners.docker]` make sure that `pull_policy`
  equals to `"if-not-present"`. This setting will make the gitlab-runner search the image locally.

.. _guide: https://docs.gitlab.com/runner/install/linux-manually.html

------------------
Setup GitLab Pages
------------------
`GitLab Pages`_ is a cool feature that allows you to create a static web server on top of gitlab (under
the `https://gitlab.io` domain).

We'll use this service to host our generated documentation website.

GitLab Pages will be enabled once you deploy your website to it and the website will be available at ``https://<username>.gitlab.io/<project_name>``.
On the next section you'll learn how to deploy your website.


----------------------
CI/CD of documentation
----------------------
In this section we'll deploy our documentation site in a CI/CD method.

**Exercise:**

* Follow `this guide <https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html>`_ and improve your `.gitlab-ci.yml` file so now it will deploy the documentation website to ``https://<username>.gitlab.io/<project_name>``.

:Note: Your site will be available up-to 30 minutes after the first deployment.





